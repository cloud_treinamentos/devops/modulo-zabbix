# Suba uma instancia no RDS para ser o banco de dados e enquanto a instancia é inicializada faça as etapas seguintes

# Suba a instância no EC2 com t2.micro e 10gb armazenamento

# Faça o download do arquivo que irá instalar o repositório no ambiente
wget https://repo.zabbix.com/zabbix/6.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-4%2Bubuntu20.04_all.deb

# Vamos realizar a instalação do .deb para que assim tenhamos o repositório
sudo dpkg -i zabbix-release_6.0-4+ubuntu20.04_all.deb

# Vamos atualizar nossos repositórios
sudo apt update

# Vamos efetivamente instalar o Zabbix
sudo apt install -y zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-sql-scripts zabbix-agent

# Dentro da instancia execute o comando abaixo adequanda-o ao seu ambiente
mysql -h <DB_ENDPOINT> -u<DB_USER> -p<DB_PASSWORD>

# Crie o banco de dados
create database zabbix character set utf8 collate utf8_bin;

# Saia do cli do MySQL
quit;

# No servidor do Zabbix, importe o esquema inicial e os dados.
zcat /usr/share/zabbix-sql-scripts/mysql/server.sql.gz | mysql -h <DB_ENDPOINT> -u<DB_USER> -p<DB_PASSWORD> zabbix

# Editar arquivo /etc/zabbix/zabbix_server.conf
sudo nano /etc/zabbix/zabbix_server.conf

# Editar as informações abaixo conforme seu ambiente:
# ANTES | DEPOIS
DBUser=zabbix          | DBUser=<DB_USER>
# DBPassword=          | DBPassword=<DB_PASS>
# DBHost=localhost     | DBHost=<DB_HOST>

# Configure o PHP para o frontend Zabbix
sudo nano /etc/zabbix/apache.conf

# Editar as informações abaixo conforme seu ambiente:
# ANTES | DEPOIS
# php_value date.timezone Europe/Riga | php_value date.timezone America/Sao_Paulo

#Habilitar o idioma português para instalação do WEB
locale-gen pt_BR.UTF-8

# Inicie o servidor Zabbix e os processos do agente
sudo systemctl restart zabbix-server zabbix-agent apache2
sudo systemctl enable zabbix-server zabbix-agent apache2

# Agora acesse a interface web e faça a configuração do Frontend
http://SERVER_IP/zabbix

# Após tudo configurado acesse com o usuário "Admin" e a senha "zabbix"

# Altere a senha padrão por questões de segurança